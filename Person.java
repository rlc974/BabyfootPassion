import java.util.Random;

/**
 * 
 * @author laurent.renambatz version 1.0 Put a pair number of persons
 *
 */
public enum Person {
	AMAR, FRED, LAURENT, THOMAS, TRUC, SYAID, BENOIT_PIERRE, PASCAL;
	public final static int NUMBER_OF_TEAMS = 4; // To update

	public static Person[][] getTeam() {
		Person[][] team = new Person[1][2];
		team[0][0] = getRandomPlayer();
		team[0][1] = getPartner(team[0][0]);
		return team;
	}

	/**
	 * 
	 * @return Get a randon player
	 */
	private static Person getRandomPlayer() {
		Random random = new Random();
		return Person.values()[random.nextInt(Person.values().length)];
	}

	/**
	 * 
	 * @param player
	 *            Player one
	 * @return Get a player different from player one
	 */
	private static Person getPartner(Person player) {
		Person player2;
		do {
			player2 = getRandomPlayer();
		} while (player.equals(player2));
		return player2;
	}
}
