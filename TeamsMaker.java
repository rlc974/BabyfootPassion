import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author laurent.renambatz
 * @version 1.0
 *
 */
public class TeamsMaker {

	private Map<Person, Person> teams;

	public TeamsMaker() {
		teams = new HashMap<Person, Person>();
		buildTeams();
	}

	/**
	 * 
	 * @return true if team exist
	 */
	private boolean areInATeam(Person player1, Person player2) {
		return isInTeam(player1) && isInTeam(player2);
	}

	/**
	 * This method build team for the day
	 */
	private void buildTeams() {
		Map<Person, Person> teamsTmp = new HashMap<Person, Person>();

		boolean build = true;

		while (build) {
			Person[][] teamTmp = new Person[1][2];

			/** Load a team and check if it exists */
			teamTmp = Person.getTeam();
			Person player1 = teamTmp[0][0];
			Person player2 = teamTmp[0][1];
			if (areInATeam(player1, player2)) {
				teams.put(player1, player2);
			}

			/** Stop criteria */
			if (teamsTmp.equals(teams) && Person.NUMBER_OF_TEAMS == teams.size()) {
				build = false;
			} else {
				teamsTmp = teams;
			}
		}
	}

	/**
	 * 
	 * @return Return team
	 */
	public Map<Person, Person> getTeams() {
		return teams;
	}

	/**
	 * Print teams
	 */
	public void printTeams() {
		System.out.println("***Teams***");
		int it = 1;
		for (Map.Entry<Person, Person> team : teams.entrySet()) {
			System.out.println("Team n° " + it);
			System.out.println("-Player 1 : " + team.getKey() + ", " + " player 2 : " + team.getValue() + "\n");
			it++;
		}
	}

	private boolean isInTeam(Person player) {
		for (Map.Entry<Person, Person> team : teams.entrySet()) {
			if (team.getKey() == player || team.getValue() == player) {
				return false;
			}
		}
		return true;
	}

}
